# From the maven image https://hub.docker.com/_/maven/
FROM maven:3-jdk-8

# Copies everything to container
COPY . .

# Run maven install as per instructions
RUN mvn install